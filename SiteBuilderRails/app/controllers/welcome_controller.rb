class WelcomeController < ApplicationController
  def index
    @account = unless session[:account_id].nil? then Account.find(session[:account_id]) else Account.new end
  end
end
