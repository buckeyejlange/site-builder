class AuthenticationController < ApplicationController
  def authenticate
    @account = Account.new(get_authenticate_params)
    logger.debug "Email sent #{@account.email}"
    accountToCheck = Account.find_by email: @account.email

    if accountToCheck.authenticate @account.password
      session[:account_id] = accountToCheck.id
    end

    respond_to do |format|
      format.html { redirect_to root_url }
    end
  end

  def logout
    session[:account_id] = nil
    respond_to do |format|
      format.html { redirect_to root_url }
    end
  end


  private 
  def get_authenticate_params
    params.require(:account).permit(:email, :password)
  end
end
