class AccountController < ApplicationController
  def create
    @account = Account.new(get_create_params)

    respond_to do |format|
      if @account.save
        logger.debug @account
        session[:account_id] = @account.id
        format.html { redirect_to root_url }
      else
        logger.debug @account.errors
        format.html { render :action => 'new' }
      end
    end
  end
  
  def new
    @account = Account.new
  end

  def read
    @account = Account.find(params[:id])
  end

  def update
    @account = Account.new(get_update_params)

    respond_to do |format|
      if @account.save
        format.html { redirect_to root_url }
      else
        format.html { render :action => 'read' }
      end
    end
  end

  def delete
    @account = Account.find(params[:id])

    respond_to do |format|
      if @account.delete
        format.html { redirect_to root_url }
      else
        format.html { render :action => 'read' }
      end
    end
  end

  private
  def get_create_params
    params.require(:account).permit(:email, :first_name, :last_name, :password, :password_confirmation)
  end

  def get_update_params
    params.require(:account).permit(:email, :first_name, :last_name, :password, :password_confirmation)
  end
end
