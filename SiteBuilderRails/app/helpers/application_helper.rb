module ApplicationHelper
  def authenticated?
    !session[:account_id].nil?
  end
end
