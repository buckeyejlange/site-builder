Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => "welcome#index"

  # Account routes
  get "account/new", to: "account#new"
  get "account/:id", to: "account#read"

  post "account/create", to: "account#create"
  post "account/delete/:id", to: "account#delete"
  post "account/update", to: "account#update"

  # Authentication routes
  post "authentication/authenticate", to: "authentication#authenticate" 
  post "authentication/logout", to: "authentication#logout"
end
