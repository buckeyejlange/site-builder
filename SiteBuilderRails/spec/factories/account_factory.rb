require 'bcrypt'

FactoryGirl.define do
  factory :account do
    first_name "Test"
    last_name "User"
    email "test@test.com"
    password "password"
    password_confirmation "password"
    password_digest BCrypt::Password.create("password")
  end
end
