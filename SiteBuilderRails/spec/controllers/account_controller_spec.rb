require 'rails_helper'

describe "AccountController", type: :controller do
  before do
    @controller = AccountController.new
  end

  describe "create" do
    context "account is valid" do
      let(:valid_account) do 
        { 
          :first_name => 'Account', 
          :last_name => 'Controller', 
          :email => 'Account@create.test', 
          :password => 'test', 
          :password_confirmation => 'test' 
        }
      end

      # NOTE: I could make the post call in the subject, but 
      # this seems to clear out the session before the 'it' calls
      # can get to it, so instead I'm calling them explicitly.
      it "should redirect to root" do 
        post :create, params: { account: valid_account }
        expect(@response).to redirect_to root_url
      end

      it "should write to session" do
        post :create, params: { account: valid_account }
        expect(session[:account_id]).not_to be_nil
      end
    end

    context "accounts passwords do not match" do
      let(:bad_pass_account) do 
        { 
          :first_name => 'Account', 
          :last_name => 'Controller', 
          :email => 'Account@create.test', 
          :password => 'test', 
          :password_confirmation => 'notmatch-test' 
        }
      end

      subject { post :create, params: { account: bad_pass_account } }

      it { is_expected.to have_http_status(200)  }
    end

    context "account is missing email" do
      let(:no_email_account) do 
        { 
          :first_name => 'Account', 
          :last_name => 'Controller', 
          :password => 'test', 
          :password_confirmation => 'test' 
        }
      end

      subject { post :create, params: { account: no_email_account } }

      it { is_expected.to have_http_status(200)  }
    end
  end

  describe "read" do
    context "id is valid" do
      let(:valid_account) do
        create(:account, :email => 'Account@read.test')
      end

      subject { get :read, :id => valid_account.id }

      it { is_expected.to have_http_status(200) }
    end

    context "id is not valid" do
      it "raises error" do
        expect { get :read, :id => 0 }.to raise_error 
      end
    end
  end

  describe "update" do
    context "update is valid" do
      let(:valid_account) do
        create(:account, :email => 'Account@update.test')
      end

      let(:valid_account_params) do
        { 
          :id => valid_account.id, 
          :first_name => 'Update worked!',
          :last_name => 'test',
          :email => valid_account.email,
          :password => valid_account.password,
          :password_confirmation => valid_account.password_confirmation
        }
      end

      subject { post :update, params: { account: valid_account_params } }

      it { is_expected.to redirect_to root_url } 
    end

    context "account does not exist" do
      let(:no_account_params) do
        { 
          :id => valid_account.id, 
          :first_name => 'Update worked!',
          :last_name => 'test',
          :email => valid_account.email,
          :password => valid_account.password,
          :password_confirmation => valid_account.password_confirmation
        }
      end

      it "is expected to raise error" do
        expect { post :update, params: { account: no_account_params } }.to raise_error
      end
    end
  end

  describe "delete" do
    context "id is valid" do
      let(:valid_account) do
        create(:account, :email => 'Account@delete.test')
      end

      subject { post :delete, :id => valid_account.id }

      it { is_expected.to redirect_to root_url }
    end

    context "id is not valid" do
      it "raises error" do
        expect { post :delete, :id => 0 }.to raise_error 
      end
    end
  end
end
