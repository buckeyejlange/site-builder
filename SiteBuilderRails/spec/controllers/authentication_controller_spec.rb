require 'rails_helper'

describe "AuthenticationController", type: :controller do
  before do
   @controller = AuthenticationController.new
  end

  describe "authenticate" do
    context "passwords match" do
      before do
        create(:account, :email => 'Authenticate@login.test')
      end

      let(:match_params) do
        { :email => 'Authenticate@login.test', :password => 'password' }
      end

      subject { post :authenticate, params: { account: match_params } }

      it "should save to session" do
        post :authenticate, params: { account: match_params }
        expect(session[:account_id]).to_not be_nil
      end

      it { is_expected.to redirect_to root_url }
    end

    context "paswords do not match" do
      before do
        create(:account, :email => 'Authenticate@login.test')
      end

      let(:no_match_params) do
        { :email => 'Authenticate@login.test', :password => 'badpass' }
      end

      subject { post :authenticate, params: { account: no_match_params } }

      it "should not save to session" do
        post :authenticate, params: { account: no_match_params }
        expect(session[:account_id]).to be_nil
      end
  
      it { is_expected.to redirect_to root_url }
    end
  end

  describe "logout" do
    it "should clear session" do
      session[:account_id] = 15
      post :logout
      expect(session[:account_id]).to be_nil
    end
  end
end
