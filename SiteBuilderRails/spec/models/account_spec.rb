require 'rails_helper'

describe "Account" do
  it "has secure password" do
    account = Account.new
    expect(account).to respond_to(:password_digest)
    expect(account).to respond_to(:authenticate)
  end

  it "should not save without email" do
    account = Account.new
    account.first_name = "this"
    account.last_name = "should fail"
    
    expect(account.save).to be(false)
  end
end

